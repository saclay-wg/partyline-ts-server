# partyline-ts-server

NodeJS package to interface with partyline

This package is an interface for [the PartyLine API Specification](https://app.swaggerhub.com/apis/HadrienRenaud/partyline)

Code Source on [gitlab.com](https://gitlab.com/saclay-wg/partyline-ts-server/blob/master/lib/types.ts)

## API

### PartyLineRouter

This is an express router, which has to be used like this :

```
var app = express()
PartyLineRouter(app)

app.listen(8080, () => {
  console.log("PartyLine server running on port 8080")
})
```

This is a typescript class that can be supercharged. The following methods are interesting, and this is their current implementation :
```ts
async getUser(id: string): Promise<PLUser> {
  return PLExamples.user
}

async getGroup(id: string): Promise<PLGroup> {
  return PLExamples.group
}

async getPost(id: string): Promise<PLPost> {
  return PLExamples.post
}

async deletePost(id: string): Promise<Status> {
  console.info(`DELETE ${id}. No action done.`);
  return Status.NotImplemented;
}


async newPost(post: PLPost): Promise<Status> {
  console.info("NEW post: ", post)
  return Status.NotImplemented
}


async editPost(post: PLPost): Promise<Status> {
  console.info("EDIT post: ", post)
  return Status.NotImplemented
}


async commentPost(post: PLPost, id: string): Promise<Status> {
  console.info(`COMMENT on post ${id}: `, post)
  return Status.NotImplemented
}


async commentEvent(post: PLPost, id: string): Promise<Status> {
  console.info(`COMMENT on Event ${id}: `, post)
  return Status.NotImplemented
}


async newEvent(event: PLEvent) {
  console.info(`NEW EVENT:`, event)
  return Status.NotImplemented
}


async getEvent(id: string): Promise<PLEvent> {
  return PLExamples.event
}

async deleteEvent(id: string): Promise<Status> {
  console.info("DELETE event: ", id)
  return Status.NotImplemented
}

async getEvents(ts: string): Promise<PLEvent[]> {
  return [PLExamples.event, PLExamples.event, PLExamples.event]
}

async editEvent(event: PLEvent): Promise<Status> {
  console.log("EDIT event :", event)
  return Status.NotImplemented
}
```

### PLUser, PLGroup, PLAuthor, PLEvent, PLPost

Those interfaces are defined in [types.ts](https://gitlab.com/saclay-wg/partyline-ts-server/blob/master/lib/types.ts) in accordance with the [the specification](https://app.swaggerhub.com/apis-docs/HadrienRenaud/partyline)

### PartyLineServer

A function that instances an express server, configures it with PartyLineRouter, and returns it.


Usage :
```ts
var app = PartyLineServer()
```
