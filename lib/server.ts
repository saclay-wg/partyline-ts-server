import { Application, NextFunction, Request, Response } from 'express';

import { version } from './config';
import { PLEvent, PLExamples, PLGroup, PLPost, PLUser } from './types';

export class PartyLineSender {
  async send(data: any) {
    return false
  }
}

enum Status {
  Ok = 200,
  NotImplemented = 501,
}

export class PartyLineRouter {
  private baseUrl = '/api/partyline/' + version;

  setupRoutes(app: Application): Application {
    app.get(this.baseUrl + '/user/:id', this.routeGetUser.bind(this))
    app.get(this.baseUrl + '/group/:id', this.routeGetGroup.bind(this))
    app.get(this.baseUrl + '/posts/:id', this.routeGetPost.bind(this))
    app.delete(this.baseUrl + '/posts/:id', this.routeDeletePost.bind(this))
    app.post(this.baseUrl + '/posts/new', this.routeNewPost.bind(this))
    app.patch(this.baseUrl + '/posts/edit', this.routeEditPost.bind(this))
    app.post(this.baseUrl + '/posts/respond/:id', this.routeCommentPost.bind(this))
    app.post(this.baseUrl + "/events/respond/:id", this.routeCommentEvent.bind(this))
    app.post(this.baseUrl + '/events/new', this.routeNewEvent.bind(this))
    app.get(this.baseUrl + '/events/:id', this.routeGetEvent.bind(this))
    app.delete(this.baseUrl + '/events/:id', this.routeDeleteEvent.bind(this))
    app.get(this.baseUrl + '/events', this.routeGetEvents.bind(this))
    app.patch(this.baseUrl + "/events/edit", this.routeEditEvent.bind(this))

    app.get(this.baseUrl + '/docs?u?(ment)?(ation)?', this.redirectToDocs)

    return app
  }

  private redirectToDocs(req: Request, res: Response) {
    res.redirect("https://app.swaggerhub.com/apis-docs/HadrienRenaud/partyline/0.1.1")
  }

  private async routeGetUser(req: Request, res: Response, next: NextFunction) {
    res.json(await this.getUser(req.params.id));
  }

  async getUser(id: string): Promise<PLUser> {
    return PLExamples.user
  }

  private async routeGetGroup(req: Request, res: Response, next: NextFunction) {
    res.json(await this.getGroup(req.params.id));
  }

  async getGroup(id: string): Promise<PLGroup> {
    return PLExamples.group
  }

  private async routeGetPost(req: Request, res: Response, next: NextFunction) {
    res.json(await this.getPost(req.params.id))
  }

  private async getPost(id: string): Promise<PLPost> {
    return PLExamples.post
  }

  private async routeDeletePost(req: Request, res: Response, next: NextFunction) {
    const status = await this.deletePost(req.params.id);
    res.sendStatus(status)
  }

  async deletePost(id: string): Promise<Status> {
    console.info(`DELETE ${id}. No action done.`);

    return Status.NotImplemented;
  }

  private async routeNewPost(req: Request, res: Response, next: NextFunction) {
    const status = await this.newPost(req.body.json);
    res.sendStatus(status)
  }

  async newPost(post: PLPost): Promise<Status> {
    console.info("NEW post: ", post)

    return Status.NotImplemented
  }

  private async routeEditPost(req: Request, res: Response, next: NextFunction) {
    const status = await this.editPost(req.body.json);
    res.sendStatus(status)
  }

  async editPost(post: PLPost): Promise<Status> {
    console.info("EDIT post: ", post)

    return Status.NotImplemented
  }

  private async routeCommentPost(req: Request, res: Response, next: NextFunction) {
    const status = await this.commentPost(req.body.json, req.params.id);
    res.sendStatus(status)
  }

  async commentPost(post: PLPost, id: string): Promise<Status> {
    console.info(`COMMENT on post ${id}: `, post)

    return Status.NotImplemented
  }

  private async routeCommentEvent(req: Request, res: Response, next: NextFunction) {
    const status = await this.commentEvent(req.body.json, req.params.id);
    res.sendStatus(status)
  }

  async commentEvent(post: PLPost, id: string): Promise<Status> {
    console.info(`COMMENT on Event ${id}: `, post)

    return Status.NotImplemented
  }

  private async routeNewEvent(req: Request, res: Response, next: NextFunction) {
    const status = await this.newEvent(req.body.json)
    res.sendStatus(status)
  }

  async newEvent(event: PLEvent) {
    console.info(`NEW EVENT:`, event)

    return Status.NotImplemented
  }

  private async routeGetEvent(req: Request, res: Response, next: NextFunction) {
    const event = await this.getEvent(req.params.id)
    res.json(event)
  }

  async getEvent(id: string): Promise<PLEvent> {
    return PLExamples.event
  }

  private async routeDeleteEvent(req: Request, res: Response, next: NextFunction) {
    res.sendStatus(await this.deleteEvent(req.params.id))
  }

  async deleteEvent(id: string): Promise<Status> {
    console.info("DELETE event: ", id)

    return Status.NotImplemented
  }

  private async routeGetEvents(req: Request, res: Response, next: NextFunction) {
    res.json(await this.getEvents(req.params.ts))
  }

  async getEvents(ts: string): Promise<PLEvent[]> {
    return [PLExamples.event, PLExamples.event, PLExamples.event]
  }

  private async routeEditEvent(req: Request, res: Response, next: NextFunction) {
    res.sendStatus(await this.editEvent(req.body.json))
  }

  async editEvent(event: PLEvent) {
    console.log("EDIT event :", event)

    return Status.NotImplemented
  }
}
