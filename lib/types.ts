export interface PLUser {
  /**
   * Le nom complet de la personne
   */
  displayName: string;

  /**
   * Son adresse email d'école
   */
  email: string;

  /**
   * Une URL vers son image de profil sur le système de son école.
   */
  photo: string;

  /**
   * Son UUID (v4) sur le système de son école
   */
  uuid: string;

  /**
   * Le système qui stocke l'user
   */
  nodeId: string;
}

const exampleUser: PLUser = {
  "displayName": "Linus Torvalds",
  "email": "jacques.biot@polytechnique.edu",
  "photo": "https://ensta-data.fr/saturne/user/e06be8bf-cf83-4735-a8f2-6d6e02e5b120",
  "uuid": "e06be8bf-cf83-4735-a8f2-6d6e02e5b120",
  "nodeId": "viarezo"
}

export interface PLGroup {
  /**
   * Le nom complet du groupe
   */
  displayName: string;

  /**
   * Son adresse email
   */
  email: string;

  /**
   * Une URL vers son image de profil sur le système de son école.
   */
  photo: string;

  /**
   * Son UUID (v4) sur le système de son école
   */
  uuid: string;

  /**
   * Le système qui stocke le groupe
   */
  nodeId: string;
};

const exampleGroup: PLGroup = {
  "displayName": "WACS",
  "email": "br@binets.fr",
  "photo": "https://ensta-data.fr/saturne/group/4d7e1c58-98e7-4c02-aeac-d1c0407d7cb9",
  "uuid": "4d7e1c58-98e7-4c02-aeac-d1c0407d7cb9e",
  "nodeId": "polytechnique"
}

export interface PLAuthor {
  id: string;
  type: string;
  nodeId: string;
};

const exampleAuthor: PLAuthor = {
  "id": "27740b47-0507-40de-b215-5dfa6942f6d8",
  "type": "group",
  "nodeId": "telecom-paristech"
}

export interface PLEvent {

  /**
   * Titre canonique de l'évènement
   */
  title: string;

  /**
   * Une description en paragraphe de l'évènement. Peut contenir des sauts de ligne
   */
  description: string;

  /**
   * La localisation.Devrait pouvoir être entrée dans un service de cartographie en ligne(type Google Maps) pour faciliter la venue des invités
   */
  place: string;

  /**
   * La timestamp de début de l'évènement. Formattée selon la norme RFC 3339.
   */
  startTime: string;

  /**
   * La timestamp de fin de l'évènement. Formattée selon la norme RFC 3339.
   */
  endTime: string;

  /**
   * l'UUID de l'évènement sur le système de l'école qui l'a produit.
   */
  uuid: string;

  /**
   * Le système qui stocke l'event, et donc l'auteur de l'event
   */
  nodeId: string;

  /**
   * L'auteur de l'event
   */
  author: PLAuthor;
};

const exampleEvent: PLEvent = {
  "title": "Soirée ramassage de déchets",
  "description": "Lorem **ipsum**\\nBlablabla…",
  "place": "48.7147,2.2102",
  "startTime": "2019-03-16T23:08:58.109Z",
  "endTime": "2019-03-17T01:08:58.109Z",
  "uuid": "27740b47-0507-40de-b215-5dfa6942f6d8",
  "nodeId": "telecom-paristech",
  "author": {
    "id": "27740b47-0507-40de-b215-5dfa6942f6d8",
    "type": "group",
    "nodeId": "telecom-paristech"
  }
}

export interface PLPost {

  /**
   * Titre canonique de l'évènement
   */
  title: string;

  /**
   * Une description en paragraphe de l'évènement. Peut contenir des sauts de ligne
   */
  description: string;

  /**
   * Timestamp de publication de l'article, formattée selon la norme RFC 3339
   */
  createdAt: string;

  /**
   * Timestamp de dernière modification de l'article, formattée selon la norme RFC 3339
   */
  updatedAt: string;

  /**
   * l'UUID du post sur le système de l'école qui l'a produit.
   */
  uuid: string;

  /**
   * Le système qui stocke l'event, et donc l'auteur de l'event
   */
  nodeId: string;

  /**
   * L'auteur du post
   */
  author: PLAuthor;

  /**
   *
   */
  parent: null | {

    /**
     * Potentiel parent du post. Ce champs peut contenir un UUID vers le parent, qu'il soit de type Post ou Event.
     */
    id: string;

    /**
     * Précise le type du parent (Event ou Post)
     */
    type: string;
  }

  /**
   * Une liste d'URL de ressources distances qui seront mises à disposition avec l'article (photos, documents…)
   */
  attachements: [string]
};

const examplePost: PLPost = {
  "title": "Pizza + tarte en OPEN",
  "description": "Venez vous servir au grand Hall, il reste plein de pizza, tartes au pomme et jus de fruit provenant du forum 4A !",
  "createdAt": "2019-03-16T23:08:58.109Z",
  "updatedAt": "2019-03-16T23:08:58.109Z",
  "uuid": "5b6cbbe5-2fca-434c-9ffa-754fddd081e2",
  "nodeId": "polytechnique",
  "author": {
    "id": "27740b47-0507-40de-b215-5dfa6942f6d8",
    "type": "group",
    "nodeId": "telecom-paristech"
  },
  "parent": {
    "id": "f88f228f-5b7b-4bef-8e49-efc60f811d49",
    "type": "event"
  },
  "attachements": [
    "https://sigma.binets.fr/assets/8c1c6032-42ea-455a-a4f5-c58ee7eaced2"
  ]
};

export class PLExamples {
  static author = exampleAuthor
  static event = exampleEvent
  static group = exampleGroup
  static post = examplePost
  static user = exampleUser
}

