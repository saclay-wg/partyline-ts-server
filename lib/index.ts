import bodyParser = require('body-parser');
import express = require('express');

import { PartyLineRouter } from './server';
import { PLAuthor, PLEvent, PLGroup, PLPost, PLUser } from './types';

export { PartyLineRouter }
export { PLUser, PLGroup, PLAuthor, PLEvent, PLPost }

export function PartyLineServer(): express.Application {
  const router = new PartyLineRouter()

  const app = express()

  app.use(bodyParser());

  return router.setupRoutes(app)
}
