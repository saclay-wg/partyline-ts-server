const PartyLineServer = require('./dist/index.js').PartyLineServer
const version = require('./dist/config.js').version

const port = 8080

console.log(`Starting PartyLineServer version ${version}...`)

const app = PartyLineServer()

app.listen(port, () => {
  console.log(`Application running on port ${port}.`)
})
